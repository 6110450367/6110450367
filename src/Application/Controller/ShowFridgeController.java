package Application.Controller;

import Application.File.Food;
import Application.File.FoodFileManager;
import Application.File.RefrigeratorFood;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ShowFridgeController {
    private ArrayList<String> foodShow;
    private ChoiceBox<String> foodChoice;
    private FoodFileManager foodFileManager;
    private ArrayList<RefrigeratorFood> foods;
    private ArrayList<RefrigeratorFood> fridgeFoods;
    private ArrayList<RefrigeratorFood> freezeFoods;
    private ArrayList<Pane> fridgesNormal = new ArrayList<>();
    private ArrayList<Pane> fridgesFreeze = new ArrayList<>();


    private @FXML Button backBtn, removeBtn;
    private @FXML Pane pane;
    private @FXML StackPane stackPane;
    private @FXML Pane freezePane, normalPane;
    private @FXML Text freezeText, normalText;


    public void initialize(){
        Platform.runLater(() -> {
            File foodFile = new File("Data" + File.separator + "Food");
            foodFileManager = new FoodFileManager(foodFile);
            fridgeFoods = foodFileManager.getFridgeFood();
            freezeFoods = foodFileManager.getFreezeFood();

//            foodChoice = new ChoiceBox<>();
//            foodChoice.setPrefSize(138,32);foodChoice.setStyle("-fx-background-color: #ffffff");
//            foodChoice.setLayoutX(278);foodChoice.setLayoutY(417);
//            normalPane.getChildren().addAll(foodChoice);

            setBackBtn();
            setFridge();
            setFood();
            setRemoveFood();

            removeBtn.setOnMouseEntered(event -> removeBtn.setTextFill(Color.WHITE));
            removeBtn.setOnMouseExited(event -> removeBtn.setTextFill(Color.BLACK));
            removeBtn.setOnAction(event -> removeBtnOnAction());
        });
    }

    public void setRemoveFood(){
        normalPane.getChildren().remove(foodChoice);
        foodChoice = new ChoiceBox<>();
        foodChoice.setPrefSize(138,32);foodChoice.setStyle("-fx-background-color: #ffffff");
        foodChoice.setLayoutX(278);foodChoice.setLayoutY(417);
        normalPane.getChildren().addAll(foodChoice);

        foodChoice.getItems().addAll(foodShow);
    }

    public void removeBtnOnAction() {
        foods = foodFileManager.getFoods();
        String value = foodChoice.getValue();
        if (value == null) {
            return;
        }

        for(int i = 0; i < foods.size(); i++){
            if(foods.get(i).getName().equals(value)){
                value = foodChoice.getValue();
            }
        }

        String slot = value.split(" ")[2].trim();
        String removeFoodName = "";
        if (value.contains("freeze")) {
            for (RefrigeratorFood food : freezeFoods) {
                if (food.getSlot().equals(slot)) {
                    removeFoodName = food.getName();
                }
            }
        } else {
            for (RefrigeratorFood food : fridgeFoods) {
                if (food.getSlot().equals(slot)) {
                    removeFoodName = food.getName();
                }
            }
        }

        ArrayList<RefrigeratorFood> removeFood = foodFileManager.getFoods(removeFoodName, slot, foodFileManager.findExpEarly(removeFoodName,slot).getExpireDate());
        int left = foodFileManager.removeFood(foodFileManager.findExpEarly(removeFoodName, slot));
        ExpFood.display("Expired food", removeFood.get(0).getName() + " " + removeFood.size() + " " + removeFood.get(0).getUnit());
        foodFileManager.save();

        fridgesNormal.clear();
        fridgesFreeze.clear();

        fridgeFoods = foodFileManager.getFridgeFood();
        freezeFoods = foodFileManager.getFreezeFood();
        freezePane.getChildren().clear();
        normalPane.getChildren().clear();
        setBackBtn();
        setFridge();
        setFood();
        normalPane.getChildren().add(foodChoice);
        normalPane.getChildren().add(removeBtn);
        normalPane.getChildren().add(normalText);
        freezePane.getChildren().add(freezeText);

        if (left == 0) {
            System.out.println(11111);
            foodShow.remove(value);
            setRemoveFood();
        }

        for (String food : foodShow) {
            System.out.println(food);
        }

    }

    public void setFridge() {
        for (int i=0;i<6;i++) {
            Pane fridge = new Pane();
            fridge.setPrefSize(150,150);
            if (i<=2) {
                fridge.setLayoutX(14 + i*171);
                fridge.setLayoutY(35);
            } else {
                fridge.setLayoutY(244);
                fridge.setLayoutX(14 + (i-3)*171);
            }
            fridge.setStyle("-fx-background-color: #FFFFFF");
            normalPane.getChildren().add(fridge);
            fridgesNormal.add(fridge);
        }
        fridgesFreeze.add(new Pane());fridgesFreeze.add(new Pane());
        fridgesFreeze.get(0).setLayoutX(27);fridgesFreeze.get(0).setLayoutY(31);
        fridgesFreeze.get(1).setLayoutX(293);fridgesFreeze.get(1).setLayoutY(31);
        fridgesFreeze.get(0).setPrefSize(200,150);fridgesFreeze.get(1).setPrefSize(200,150);
        fridgesFreeze.get(0).setStyle("-fx-background-color: #FFFFFF");fridgesFreeze.get(1).setStyle("-fx-background-color: #FFFFFF");
        System.out.println(freezePane);
        System.out.println(fridgesFreeze);
        freezePane.getChildren().add(fridgesFreeze.get(0));
        freezePane.getChildren().add(fridgesFreeze.get(1));

    }

    public void setFood() {
        foodShow = new ArrayList<>();

        Collections.sort(freezeFoods, new Comparator<RefrigeratorFood>() {
            @Override
            public int compare(RefrigeratorFood o1, RefrigeratorFood o2) {
                return Integer.parseInt(o1.getSlot()) - Integer.parseInt(o2.getSlot());
            }
        });

        Collections.sort(fridgeFoods, new Comparator<RefrigeratorFood>() {
            @Override
            public int compare(RefrigeratorFood o1, RefrigeratorFood o2) {
                return Integer.parseInt(o1.getSlot()) - Integer.parseInt(o2.getSlot());
            }
        });

        for (int i = 0; i< freezeFoods.size(); i++) {
            int slot = Integer.parseInt(freezeFoods.get(i).getSlot()) -1;
            if (foodShow.indexOf("freeze slot " + (slot+1)) == -1) {
                foodShow.add("freeze slot " + (slot+1));
            }

            ImageView imageFood = new ImageView();
            File image = new File(freezeFoods.get(i).getUrlImg());
            imageFood.setImage(new Image(image.toURI().toString()));
            imageFood.setFitHeight(150);imageFood.setFitWidth(200);
            fridgesFreeze.get(slot).getChildren().add(imageFood);

            LocalDate expDate = LocalDate.parse(freezeFoods.get(i).getExpireDate());
            int amount = 0;
            for (int j=0; j<freezeFoods.size(); j++) {
                if (freezeFoods.get(j).getName().equals(freezeFoods.get(i).getName()) && freezeFoods.get(j).getSlot().equals(freezeFoods.get(i).getSlot())) {
                    amount++;
                    if (expDate.compareTo(LocalDate.parse(freezeFoods.get(j).getExpireDate())) > 0) {
                        expDate = LocalDate.parse(freezeFoods.get(j).getExpireDate());
                    }
                }
            }

            Text expText = new Text(expDate.toString());
            expText.setFill(Color.WHITE);
            expText.setFont(Font.font("Arial Black",13));
            expText.setLayoutY(196);
            if (slot==0) {
                expText.setLayoutX(27);
            } else {
                expText.setLayoutX(293);
            }
            expText.setWrappingWidth(200);expText.setTextAlignment(TextAlignment.CENTER);
            freezePane.getChildren().add(expText);

            Text name = new Text(freezeFoods.get(i).getName() + " " + amount + " " + freezeFoods.get(i).getUnit());
            name.setFont(Font.font("Arial Black",13));
            name.setLayoutX(-5);name.setLayoutY(146);
            name.setWrappingWidth(200);name.setTextAlignment(TextAlignment.CENTER);
            fridgesFreeze.get(slot).getChildren().add(name);
        }

        for (int i = 0; i < fridgeFoods.size(); i++) {
            int slot = Integer.parseInt(fridgeFoods.get(i).getSlot()) - 1;
            if (foodShow.indexOf("fridge slot " + (slot+1)) == -1) {
                foodShow.add("fridge slot " + (slot+1));
            }

            ImageView imageFood = new ImageView();
            File image = new File(fridgeFoods.get(i).getUrlImg());
            imageFood.setImage(new Image(image.toURI().toString()));
            imageFood.setFitHeight(150);imageFood.setFitWidth(150);
            fridgesNormal.get(slot).getChildren().add(imageFood);

            LocalDate expDate = LocalDate.parse(fridgeFoods.get(i).getExpireDate());
            int amount = 0;
            for (int j=0; j<fridgeFoods.size(); j++) {
                if (fridgeFoods.get(j).getName().equals(fridgeFoods.get(i).getName()) && fridgeFoods.get(j).getSlot().equals(fridgeFoods.get(i).getSlot())) {
                    amount++;
                    if (expDate.compareTo(LocalDate.parse(fridgeFoods.get(j).getExpireDate())) > 0) {
                        expDate = LocalDate.parse(fridgeFoods.get(j).getExpireDate());
                    }
                }
            }

            Text expText = new Text(expDate.toString());
            expText.setFill(Color.WHITE);
            expText.setFont(Font.font("Arial Black",13));
            if (slot < 3) {
                expText.setLayoutY(200);
                expText.setLayoutX(9 + slot*171);
            } else {
                expText.setLayoutY(410);
                expText.setLayoutX(9 + (slot-3)*171);
            }
            expText.setWrappingWidth(160);expText.setTextAlignment(TextAlignment.CENTER);
            normalPane.getChildren().add(expText);

            Text name = new Text(fridgeFoods.get(i).getName() + " " + amount + " " + fridgeFoods.get(i).getUnit());
            name.setFont(Font.font("Arial Black",13));
            name.setLayoutX(-5);name.setLayoutY(146);
            name.setWrappingWidth(160);name.setTextAlignment(TextAlignment.CENTER);
            fridgesNormal.get(slot).getChildren().add(name);
        }


    }

    public void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("homepage.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(),0 , Interpolator.EASE_IN);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();

                }catch (IOException e) {e.printStackTrace();}
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setTextFill(Color.WHITE));
        backBtn.setOnMouseExited(event -> backBtn.setTextFill(Color.BLACK));
    }

}
