package Application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Refrigerator extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Controller/homepage.fxml"));
        primaryStage.setTitle("Refrigerator");
        primaryStage.setScene(new Scene(root, 570, 790));
        primaryStage.show();
        primaryStage.setResizable(false);

    }
    public static void main(String[] args) {
        launch(args);
    }
}