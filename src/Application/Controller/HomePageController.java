package Application.Controller;


import Application.File.TypeFileManager;
import Application.File.Type;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class HomePageController {
    private ArrayList<Type> types = new ArrayList<>();
    private TypeFileManager typeFileManagers;
    private DropShadow dropShadow = new DropShadow();
    private Timeline timeline = new Timeline();
    private String type;

    @FXML private Button creditBtn, fridgeBtn;
    @FXML private Pane pane;
    @FXML private StackPane stackPane;

    public void initialize() {
        Platform.runLater(() ->{
            File type = new File("Data" + File.separator + "Types");
            typeFileManagers = new TypeFileManager(type);

            setTypes();
            typeFileManagers.setTypes(types);
            typeFileManagers.save();
            setType();
            setCreditBtn();
            setFridgeBtn();
        });
    }

    public void setFridgeBtn(){
        fridgeBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("showFridge.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();
                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                }catch (IOException e) {e.printStackTrace();}
            }
        });
        fridgeBtn.setOnMouseEntered(event -> fridgeBtn.setTextFill(Color.WHITE));
        fridgeBtn.setOnMouseExited(event -> fridgeBtn.setTextFill(Color.BLACK));
    }

    private void setTypes(){
        types.add(new Type("fruit"));
        types.add(new Type("dessert"));
        types.add(new Type("freeze food"));
        types.add(new Type("vegetable"));
        types.add(new Type("meat"));
        types.add(new Type("drinks"));
    }

    private void setType(){
        ArrayList<ImageView> imageType = new ArrayList<>();
        ArrayList<Button> addBtn = new ArrayList<>();
        for(int i = 0; i < types.size(); i++){
            addBtn.add(new Button(types.get(i).getTitle()));
            addBtn.get(i).setFont(Font.font("Arial Black",15));
            addBtn.get(i).setStyle("-fx-background-color :  #ff5757");
            addBtn.get(i).setEffect(dropShadow);
            addBtn.get(i).setMinWidth(150);
            addBtn.get(i).setMaxHeight(15);
            addBtn.get(i).setPadding(new Insets(2));
            int finalI = i;
            addBtn.get(i).setOnMouseEntered(event -> {
                addBtn.get(finalI).setStyle("-fx-background-color: #cc0000; -fx-text-fill: white;");
                addBtn.get(finalI).setPadding(new Insets(3));
            });
            addBtn.get(i).setOnMouseExited(event -> {
                addBtn.get(finalI).setStyle("-fx-background-color: #ff5757; -fx-text-fill: black");
                addBtn.get(finalI).setPadding(new Insets(2));
            });
            File image = new File(types.get(i).getUrlImage());
            imageType.add(new ImageView(new Image(image.toURI().toString())));
            imageType.get(i).setFitWidth(150);
            imageType.get(i).setFitHeight(150);
            imageType.get(i).setEffect(dropShadow);
            imageType.get(i).setOnMouseEntered(event -> {
                addBtn.get(finalI).setStyle("-fx-background-color: #cc0000; -fx-text-fill: white;");
                addBtn.get(finalI).setPadding(new Insets(3));
            });
            imageType.get(finalI).setOnMouseExited(event -> {
                addBtn.get(finalI).setStyle("-fx-background-color: #ff5757; -fx-text-fill: black");
                addBtn.get(finalI).setPadding(new Insets(2));
            });
            if (i <= 2) {
                addBtn.get(i).setLayoutX(40 + i * 170);
                addBtn.get(i).setLayoutY(410);
                imageType.get(i).setLayoutX(40 + i * 170);
                imageType.get(i).setLayoutY(260);
            } else {
                addBtn.get(i).setLayoutX(40 + (i - 3) * 170);
                addBtn.get(i).setLayoutY(609);
                imageType.get(i).setLayoutX(40 + (i - 3) * 170);
                imageType.get(i).setLayoutY(459);
            }
            addBtn.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    type = addBtn.get(finalI).getText();
                    timeline.pause();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("receivefood.fxml"));
                        Parent root = loader.load();
                        Scene scene = pane.getScene();
                        ReceiveFoodController receiveFoodController = loader.getController();
                        receiveFoodController.setReceive(type);

                        root.translateYProperty().set(scene.getHeight() * -1);
                        stackPane.getChildren().add(root);

                        Timeline timeline = new Timeline();
                        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                        KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                        timeline.getKeyFrames().add(kf);
                        timeline.setOnFinished(t -> {
                            stackPane.getChildren().remove(pane);
                        });
                        timeline.play();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            pane.getChildren().add(imageType.get(i));
            pane.getChildren().add(addBtn.get(i));
        }
    }

    public void setCreditBtn() {
        creditBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("profile.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {e.printStackTrace();}
            }
        });
        creditBtn.setOnMouseEntered(event -> creditBtn.setTextFill(Color.WHITE));
        creditBtn.setOnMouseExited(event -> creditBtn.setTextFill(Color.BLACK));
    }

}
