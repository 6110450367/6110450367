package Application.File;

import java.io.File;

public class Type {
    private String title;
    private String urlImage;

    public Type(String title){
        this.title = title;
        this.urlImage = "Data" + File.separator + "Types" + File.separator + title + File.separator + "type.jpg";

    }

    public String getTitle() {
        return title;
    }

    public String getUrlImage() {
        return urlImage;
    }

    @Override
    public String toString() {
        return title+"";
    }
}
