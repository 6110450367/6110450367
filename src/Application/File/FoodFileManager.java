package Application.File;

import Application.Refrigerator;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class FoodFileManager {
    private ArrayList<RefrigeratorFood> foods;
    private File foodDir;


    public FoodFileManager(File foodDir) {
        this.foodDir = foodDir;
        File foodList = new File(foodDir.getPath() + File.separator + "FoodList.csv");
        if(!foodList.exists()){
            try{
                foodList.createNewFile();
            }catch (IOException e){
                System.err.println("Can not create foodList file");
            }
        }
        foods = new ArrayList<>();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(foodList));
            String line;
            while ((line = buffer.readLine()) != null){
                String[] info = line.split(",");
                String name = info[0].trim();
                String unit = info[1].trim();
                LocalDate addDate = LocalDate.parse(info[2].trim());
                LocalDate expDate = LocalDate.parse(info[3].trim());
                String slot = info[4].trim();
                RefrigeratorFood food = new RefrigeratorFood(name, unit, addDate, expDate, slot);
                foods.add(food);
            }
        }catch (FileNotFoundException e){
            System.err.println("File not found.");
        }catch (IOException e){
            System.err.println("Read file error.");
        }

    }



    public void addFood(RefrigeratorFood food){
        foods.add(food);
        save();
    }

    public void save(){
        File foodList = new File(foodDir.getPath() + File.separator + "FoodList.csv");
        BufferedWriter buffer = null;
        try{
            buffer = new BufferedWriter(new FileWriter(foodList));
            for(RefrigeratorFood foodFile : foods){
                buffer.write(foodFile.toString());
                buffer.newLine();
            }
            buffer.flush();
            buffer.close();
        }catch (FileNotFoundException e){
            System.err.println("Food find not found.");
        }catch (IOException e){
            System.err.println("Food file cannot save.");
        }
    }

    public void setFoods(ArrayList<RefrigeratorFood> foods) {
        this.foods = foods;
    }

    public ArrayList<RefrigeratorFood> getFoods() {
        return foods;
    }

    public ArrayList<RefrigeratorFood> getFridgeFood() {
        ArrayList<RefrigeratorFood> foods = new ArrayList<>();
        for (RefrigeratorFood food : this.foods) {
            if (!food.isFreeze()) {
                foods.add(food);
            }
        }
        return foods;
    }

    public ArrayList<RefrigeratorFood> getFreezeFood() {
        ArrayList<RefrigeratorFood> foods = new ArrayList<>();
        for (RefrigeratorFood food : this.foods) {
            if (food.isFreeze()) {
                foods.add(food);
            }
        }
        return foods;
    }

    public ArrayList<RefrigeratorFood> getFoods(String name, String slot) {
        ArrayList<RefrigeratorFood> arrayList = new ArrayList<>();
        for (RefrigeratorFood food : foods) {
            if (food.getName().equals(name) && food.getSlot().equals(slot)) {
                arrayList.add(food);
            }
        } return arrayList;
    }

    public ArrayList<RefrigeratorFood> getFoods(String name, String slot, String expDate) {
        ArrayList<RefrigeratorFood> arrayList = new ArrayList<>();
        for (RefrigeratorFood food : foods) {
            if (food.getName().equals(name) && food.getSlot().equals(slot) && food.getExpireDate().equals(expDate)) {
                arrayList.add(food);
            }
        } return arrayList;
    }

    public RefrigeratorFood findExpEarly(String name, String slot) {
        ArrayList<RefrigeratorFood> foods = getFoods(name,slot);
        RefrigeratorFood foodEarly = foods.get(0);
        LocalDate expDate = LocalDate.parse(foods.get(0).getExpireDate());
        for (RefrigeratorFood food : foods) {
            if (expDate.compareTo(LocalDate.parse(food.getExpireDate())) > 0) {
                expDate = LocalDate.parse(food.getExpireDate());
                foodEarly = food;
            }
        } return foodEarly;
    }

    public int removeFood(RefrigeratorFood food) {
        ArrayList<RefrigeratorFood> trash = new ArrayList<>();
        for (RefrigeratorFood food1 : foods) {
            if (food1.getName().equals(food.getName()) && food1.getExpireDate().equals(food.getExpireDate())) {
                trash.add(food1);
            }
        }
        foods.removeAll(trash);
        int left = 0;
        for (RefrigeratorFood food1 : foods) {
            if (food1.getName().equals(food.getName())) {
                left++;
            }
        }
        return left;
    }
}
