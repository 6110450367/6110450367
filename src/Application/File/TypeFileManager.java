package Application.File;

import java.io.*;
import java.util.ArrayList;

public class TypeFileManager {
    private ArrayList<Type> types;
    private File type;

    public TypeFileManager(File type) {
        this.type = type;
        File typeList = new File(type.getPath() + File.separator + "TypeList.csv");

        System.out.println(type.getPath());
        System.out.println(typeList.getPath());
        if (!typeList.exists()) {
            try {
                typeList.createNewFile();

            } catch (IOException e) {
                System.err.println("Cannot create typeList file.");
            }
        }

        types = new ArrayList<>();
        try{
            BufferedReader buffer = new BufferedReader(new FileReader(typeList));
            while (buffer.readLine() != null && types.size() < 6){
                String title = type.getName();
                Type titleType = new Type(title);
                types.add(titleType);
            }
        }catch (FileNotFoundException e){
            System.err.println("File not found.");
        }
        catch (IOException e){
            System.err.println("Read file error.");
        }
    }

    public ArrayList<Type> getTypes(){return types;}


    public void setTypes(ArrayList<Type> types){this.types = types;}

    public void save(){
        File typeList = new File(type.getPath() + File.separator + "TypeList.csv");
        try{
            BufferedWriter buffer = new BufferedWriter(new FileWriter(typeList));
            for(Type type : types){
                buffer.write(type.toString());
                buffer.newLine();
            }
            buffer.flush();
            buffer.close();
        }catch (FileNotFoundException e){
            System.err.println("Type file not found.");
        }catch (IOException e){
            System.err.println("Type file cannot save.");
        }
    }
}
