package Application.Controller;

import Application.File.Food;
import Application.File.FoodFileManager;

import Application.File.RefrigeratorFood;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ReceiveFoodController {
    private ArrayList<RefrigeratorFood> foods;
    private ChoiceBox<String> nameChoice, amountChoice, slotChoice;
    private TextField unit;
    private FoodFileManager foodFileManager;
    private String type;

    @FXML private Button backBtn, addBtn, fridgeBtn;
    @FXML private StackPane stackPane;
    @FXML private Pane pane;
    @FXML private Text errText;
    @FXML private DatePicker datePicker;

    public void initialize(){
        Platform.runLater(() -> {
            File food = new File("Data" + File.separator + "Food");
            foodFileManager = new FoodFileManager(food);
            setFridgeBtn();
            setBackBtn();
            setAddBtn();

            datePicker.setDayCellFactory(picker -> new DateCell() {
                public void updateItem(LocalDate date, boolean empty) {
                    super.updateItem(date, empty);
                    LocalDate today = LocalDate.now();

                    setDisable(empty || date.compareTo(today) < 0 );
                }
            });
        });

    }

    public void handleAddBtn(Event event){
        System.out.println(datePicker.getValue());

        errText.setText("");
        errText.setVisible(false);
        errText.setLayoutX(265);
        if(nameChoice.getValue() == null){
            errText.setText("please select name");
            errText.setVisible(true);
        }else if(amountChoice.getValue() == null){
            errText.setLayoutX(265);
            errText.setText("please select amount");
            errText.setVisible(true);
        }else if(datePicker.getValue() == null){
            errText.setLayoutX(267);
            errText.setText("please select exp");
            errText.setVisible(true);
        }else if(slotChoice.getValue() == null){
            errText.setLayoutX(265);
            errText.setText("please select slot");
            errText.setVisible(true);
        } else{
            for (int i=0; i<Integer.parseInt(amountChoice.getValue()); i++) {
                RefrigeratorFood food = new RefrigeratorFood(nameChoice.getValue(), unit.getText(), LocalDate.now(), datePicker.getValue(), slotChoice.getValue());
                foodFileManager.addFood(food);
            }
        }
    }

    public void setAddBtn(){
        addBtn.setOnAction(this::handleAddBtn);
        addBtn.setOnMouseEntered(event -> addBtn.setTextFill(Color.WHITE));
        addBtn.setOnMouseExited(event -> addBtn.setTextFill(Color.BLACK));
    }

    public void setReceive(String type){
        this.type = type;
        nameChoice = new ChoiceBox<>();
        nameChoice.setLayoutX(272);
        nameChoice.setLayoutY(263);
        nameChoice.setPrefSize(138,32);
        nameChoice.setStyle("-fx-background-color: #ffffff");

        amountChoice = new ChoiceBox<>();
        amountChoice.setLayoutX(272);
        amountChoice.setLayoutY(303);
        amountChoice.setPrefSize(138,32);
        amountChoice.setStyle("-fx-background-color: #ffffff");
        amountChoice.getItems().addAll("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

        unit = new TextField();
        unit.setEditable(false);
        unit.setLayoutX(272);
        unit.setLayoutY(343);
        unit.setPrefSize(138,32);
        unit.setStyle("-fx-background-color: #ffffff");

        slotChoice = new ChoiceBox<>();
        slotChoice.setLayoutX(272);
        slotChoice.setLayoutY(423);
        slotChoice.setPrefSize(138,32);
        slotChoice.setStyle("-fx-background-color: #ffffff");
        slotChoice.setDisable(true);

        if(type.equals("fruit")){
            nameChoice.getItems().addAll("banana", "durian", "grape", "mango", "melon", "orange", "pineapple");
        }else if(type.equals("dessert")){
            nameChoice.getItems().addAll("brownie", "cake", "cookie", "egg tart", "pudding");
        }else if(type.equals("freeze food")){
            nameChoice.getItems().addAll("chocolate", "ice", "ice-cream");
        } else if(type.equals("drinks")){
            nameChoice.getItems().addAll("beer", "coffee", "juice", "milk", "water", "tea");
        }else if(type.equals("meat")){
            nameChoice.getItems().addAll("beef", "chicken", "egg", "fish", "lamb", "pork", "shrimp");
        }else if(type.equals("vegetable")){
            nameChoice.getItems().addAll("cabbage", "carrot", "cucumber", "garlic", "mushroom", "potato", "tomato");
        }

        nameChoice.setOnAction(event -> {
            File foodFile = new File("Data" + File.separator + "Food");
            foodFileManager = new FoodFileManager(foodFile);

            pane.getChildren().remove(slotChoice);
            slotChoice = new ChoiceBox<>();
            slotChoice.setLayoutX(272);
            slotChoice.setLayoutY(423);
            slotChoice.setPrefSize(138,32);
            slotChoice.setStyle("-fx-background-color: #ffffff");
            pane.getChildren().add(slotChoice);
            if(type.equals("freeze food")){
                slotChoice.getItems().addAll("1", "2");
                foods = foodFileManager.getFreezeFood();
            }else{
                slotChoice.getItems().addAll("1", "2", "3", "4", "5", "6");
                foods = foodFileManager.getFridgeFood();
            }

            for (RefrigeratorFood food : foods) {
                if (!nameChoice.getValue().equals(food.getName())) {
                    slotChoice.getItems().remove(food.getSlot());
                }
            }

            if (slotChoice.getItems().size() == 0) {
                errText.setLayoutX(265);
                errText.setText("Slot is full.");
                errText.setVisible(true);
                addBtn.setDisable(true);
            } else {
                errText.setVisible(false);
                addBtn.setDisable(false);
            }

            unit.clear();
            if(nameChoice.getValue().equals("ice-cream")){
                unit.setText("scoop");
            }else if(nameChoice.getValue().equals("chocolate")){
                unit.setText("bar");
            }else if(nameChoice.getValue().equals("ice") ){
                unit.setText("bag");
            }else if(nameChoice.getValue().equals("egg")){
                unit.setText("egg");
            }else if(nameChoice.getValue().equals("grape")){
                unit.setText("bunch");
            }else if(type.equals("dessert")){
                unit.setText("piece");
            }else if(type.equals("drinks")){
                unit.setText("bottle");
            }else if(type.equals("fruit")){
                unit.setText("ball");
            }else if(type.equals("meat")) {
                unit.setText("kilogram");
            }else if(nameChoice.getValue().equals("mushroom") || nameChoice.getValue().equals("carrot") || nameChoice.getValue().equals("garlic") || nameChoice.getValue().equals("cabbage")){
                unit.setText("head");
            }else if(type.equals("vegetable")){
                unit.setText("ball");
            }
        });
        amountChoice.setOnAction(event -> {
            String s = unit.getText();
            if(amountChoice.getValue().equals("1")){
                unit.setText(s);
            }else {
                if(nameChoice.getValue().equals("ice-cream")){
                    unit.setText("scoops");
                }else if(nameChoice.getValue().equals("chocolate")){
                    unit.setText("bars");
                }else if(nameChoice.getValue().equals("ice") ){
                    unit.setText("bags");
                }else if(nameChoice.getValue().equals("egg")){
                    unit.setText("eggs");
                }else if(nameChoice.getValue().equals("grape")){
                    unit.setText("bunches");
                }else if(type.equals("dessert")){
                    unit.setText("pieces");
                }else if(type.equals("drinks")){
                    unit.setText("bottles");
                }else if(type.equals("fruit")){
                    unit.setText("balls");
                }else if(type.equals("meat")) {
                    unit.setText("kilograms");
                }else if(nameChoice.getValue().equals("mushroom") || nameChoice.getValue().equals("carrot") || nameChoice.getValue().equals("garlic") || nameChoice.getValue().equals("cabbage")){
                    unit.setText("heads");
                }else if(type.equals("vegetable")){
                    unit.setText("balls");
                }
            }
        });
        pane.getChildren().addAll(nameChoice, amountChoice, unit, slotChoice);
    }


    public void setFridgeBtn(){
        fridgeBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("showFridge.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();
                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                }catch (IOException e) {e.printStackTrace();}
            }
        });
        fridgeBtn.setOnMouseEntered(event -> fridgeBtn.setTextFill(Color.WHITE));
        fridgeBtn.setOnMouseExited(event -> fridgeBtn.setTextFill(Color.BLACK));
    }

    public void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("homepage.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(),0 , Interpolator.EASE_IN);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();

                }catch (IOException e) {e.printStackTrace();}
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setTextFill(Color.WHITE));
        backBtn.setOnMouseExited(event -> backBtn.setTextFill(Color.BLACK));
    }
}
