package Application.File;

import java.time.LocalDate;

public class RefrigeratorFood extends Food implements Freezable {
    private String unit;
    private LocalDate addDate;
    private boolean freeze;
    private String slot;

    public RefrigeratorFood(String name, String unit, LocalDate addDate, LocalDate expDate, String slot) {
        super(name, expDate);
        this.unit = unit;
        this.addDate = addDate;
        freeze = false;
        this.slot = slot;
        if (name.equals("ice") || name.equals("ice-cream") || name.equals("chocolate")) {
            setFreeze(true);
        }
    }

    public String getUnit() {
        return unit;
    }


    public String getAddDate() {
        return addDate.toString();
    }

    @Override
    public boolean isFreeze() {
        return freeze;
    }


    public void setFreeze(boolean freeze) {
        this.freeze = freeze;
    }

    public String getSlot() {
        return slot;
    }

    @Override
    public String toString() {
        return getName() + "," + getUnit() + "," + getAddDate() + "," + getExpireDate() + "," + slot;
    }
}