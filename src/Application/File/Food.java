package Application.File;

import java.io.File;
import java.time.LocalDate;

public class Food {
    private String name;
    private LocalDate expDate;
    private String urlImg;

    public Food(String name, LocalDate expDate) {
        this.name = name;
        this.urlImg = "Data" + File.separator + "Food" +  File.separator + "Image" + File.separator + name + ".png";
        this.expDate = expDate;
    }


    public String getName() {
        return name;
    }


    public String getExpireDate() { return expDate.toString(); }


    public String getUrlImg() {
        return urlImg;
    }


}
